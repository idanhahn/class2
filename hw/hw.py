# Small Uber prepare data

import fileinput as fi
import sys as sys
import argparse as argp;


# input arguments:
parser = argp.ArgumentParser()

parser.add_argument('-num_of_sections', type=int, default= 4)
parser.add_argument('-nw_coords', type=str, default='40.764785,-73.998945')
parser.add_argument('-src_file', type=str, default='./data/uber.csv')
parser.add_argument('-dst_dir', type=str, default='./results')

args = parser.parse_args()

numOfSections = args.num_of_sections
if (numOfSections not in [2,4,8]):
    sys.exit('num_of_sections must be either 2,4,8')

nwLat, nwLng = map(float, args.nw_coords.split(','))

srcFile = args.src_file

dstDir = args.dst_dir


rows = int(numOfSections / 2)
cols = 2

sections = [[0 for x in range(rows)] for y in range(cols)]

for row in range(rows):
    for col in range(cols):
        sections[col][row] = {
            'nwLat': nwLat - row*0.008901,
            'nwLng': nwLng + col*0.011947,
            'dstFile': dstDir + "/" + str(col) + "-" + str(row),
            'cnt': 0
        }


def isInSquare(lat, lng, section):
    return ((lat < section['nwLat']) and (lat > section['nwLat'] - 0.008901))and ((lng > section['nwLng']) and (lng < section['nwLng'] + 0.011947))


def getQuarter(lat, lng, section):
    latMid = section['nwLat'] - 0.008901 / 2
    lngMid = section['nwLng'] + 0.011947 / 2

    if (lat > latMid and lng > lngMid):
        return 0
    elif (lat > latMid and lng <= lngMid):
        return 1
    elif (lat < latMid and lng > lngMid):
        return 2
    else:
        return 3


def getSection(lat, lng):
    for row in range(rows):
        for col in range(cols):
            if isInSquare(lat,lng, sections[col][row]):
                return True, getQuarter(lat, lng, sections[col][row]), col, row, sections[col][row]
    return False, 0, 0, 0, 0



def process_data():

    readLineCount = 0

    dfp = [[0 for x in range(rows)] for y in range(cols)]
    for row in range(rows):
        for col in range(cols):
            dfp[col][row] = open(sections[col][row]['dstFile'], 'w')

    for line in fi.input(srcFile):

        readLineCount += 1

        # skip for first line
        if (readLineCount == 1):
            continue

        # for any other line
        line_split = line.split(',')
        lat = float(line_split[1])
        lng = float(line_split[2])
        qResult, q, col, row, section = getSection(lat, lng)
        if (qResult):
            dfp[col][row].write(line_split[0] + "," + str(lat) + "," + str(lng) + "," + str(q) + "\n")
            section['cnt'] = section['cnt'] + 1

    print(sections)

    for row in range(rows):
        for col in range(cols):
            dfp[col][row].close()


# Main
process_data()
