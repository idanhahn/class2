# Small Uber prepare data

import fileinput as fi
import sys as sys
import argparse as argp;
import zipfile as zf

# input arguments:
parser = argp.ArgumentParser()

parser.add_argument('-src_file', type=str, default='./data/uber.csv')
parser.add_argument('-dst_dir', type=str, default='./results')

args = parser.parse_args()

srcFile = args.src_file
dstDir = args.dst_dir


inputZip = zf.ZipFile(srcFile, 'r')

fixed_file = open(dstDir + '/fixed_file.csv', 'a')
error_file = open(dstDir + '/error_file.csv', 'a')



def check_line(line):
    if len(line) == 4:
        return True
    else:
        return False


def process_data():

    readLineCount = 0

    for line in fi.input():

        readLineCount += 1

        # skip for first line
        if (readLineCount == 1):
            continue

        # for any other line
        line_split = line.split(',')

        if check_line(line_split):
            fixed_file.write(line)
        else:
            error_file.write(line)

    fixed_file.close()
    error_file.close()


# Main
process_data()
